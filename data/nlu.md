## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there
- Hello
- Hi

## intent:goodbye
- bye
- goodbye
- see you around
- see you later

## intent:yes_intent
- yes
- indeed
- of course
- that sounds good
- correct
- yeah please go ahead
- perfect go ahead
- wow go ahead
- O. K.
- okay
- yeah go ahead
- go ahead
- sure i would love to have that
- yeah i would love to have that
- sure i would love that
- perfect
- sure why not
- definitely
- yes go ahead
- yes please
- yep
- yeah please
- sure go ahead
- sure
- sure would love to
- yeah

## intent:no_intent
- no
- never
- I don't think so
- don't like that
- no way
- not really
- that's okay for now
- that is all for now
- nothing much thank you
- nothing much
- thank you that's it for now
- nothing thank you
- no that's fine
- that's it for me
- that's enough for me
- that's enough for now
- i am good that's enough
- no that's it i am good
- nope i am good
- no i am fine
- i am fine that's it
- i am fine that is it
- i am fine that would be all
- i'm fine that would be all
- i'm fine that is it
- i'm fine that's it
- no that's all
- nope that's it i am done
- nope that's it i'm done
- that's it i'm done
- that's it i am done
- no that's it i'm fine
- no that's it i am fine
- no that is it
- no that's it
- no i'm fine
- that's it i am fine thank you
- that's it i am fine thanks
- that's it thank you
- thanks that's it
- thank you that's all i want to have
- that's all i want to have
- that's it i want to have
- that's all i wanna have
- that's it i wanna have
- thank you that's it
- that's it i'm fine
- that's it i am fine
- i am fine
- thank you i am fine
- that's it
- thank you that would be so kind of you
- thank you that would be lovely
- thank you that would be much appreciated
- thanks that would be great
- thank you that would be great
- that would be all for now
- no thank you very much
- nope thank you
- no thank you that's all
- no thank you that would be all
- no thanks
- thank you
- thank you that would be all
- i am sorted
- i am done
- no i am done
- nopes that would be all
- nope that would be all
- nopes
- no that's all for now
- no thank you
- that's all for now
- that would be all
- that's all
- nopety
- nope
- nah
- no that would be all

## intent:mood_great
- perfect
- very good
- great
- amazing
- wonderful
- I am feeling very good
- I am great
- I'm good
- good
- Gud
- i am good

## intent:mood_unhappy
- sad
- very sad
- unhappy
- bad
- very bad
- awful
- terrible
- not very good
- extremely sad
- so sad
- i not good
- i am sad
- can you get some water please

## intent:bot_challenge
- are you a bot?
- are you a human?
- am I talking to a bot?
- am I talking to a human?
- Talk to hotel

## intent:amenities
- i need [1](number_1) [towel](amenity_1)
- i need [shower gel](amenity_1)
- i need [towel](amenity_1)
- i need [towel](amenity_1) and [1] (number_2) [shampoo](amenity_2)
- i need [shampoo](amenity_1)
- place an order for [2](number_1) [towel](amenity_1) along with [shampoo](amenity_2)
- place an order for [2](number_1) [towel](amenity_1) along with [1](number_2) [shampoo](amenity_2)
- place an order for [towel](amenity_1) and [shampoo](amenity_2) and [shower gel](amenity_3)
- can you send [towels](amenity_1) and [shampoo](amenity_2) and [shower gel](amenity_3)
- place and order for [towels](amenity_1) and [shampoo](amenity_2) please
- place an order for [6](number_1) [towels](amenity_1)
- i want [6](number_1) [shampoo](amenity_1)
- i need [6](number_1) [sewing kit](amenity_1)
- can you get me [1](number_1) [towel](amenity_1) and [3](number_2) [shampoo](amenity_2)
- i need [4](number_1) [dental kit](amenity_1) and [1](number_2) [sewing kit](amenity_2)
- can I get some [water](amenity_1) please
- can you please place an order for [face wipes](amenity_1)
- I need [1](number_1) [shampoo](amenity_1) and [2](number_2) [towels](amenity_2)
- i need [4](number_1) [dental kit](amenity_1)
- i need [4](number_1) [sewing kit](amenity_1)
- i need [4](number_1) [towels](amenity_1)
- can i get one more [towels](amenity_1)
- can i get one more [towels](amenity_1) please
- can you order extra [towels](amenity_1) for me
- can you order [towels](amenity_1) for me
- can i get some extra [towels](amenity_1) please
- can i have some extra [grooming kit](amenity_1) please
- can i get extra [grooming kit](amenity_1) please
- i have run out of [newspaper](amenity_1)
- can i have [newspaper](amenity_1)
- place an order for [1](number_1) [newspaper](amenity_1) along with [8] (number_2) [shampoo](amenity_2)
- place an order for [1](number_1) [mouth wash](amenity_1) along with [shampoo](amenity_2)
- place an order for [1](number_1) [mouth wash](amenity_1)
- place an order for [mouth wash](amenity_1)
- i want [1](number_1) more [mouth wash](amenity_1)
- i need [1](number_1) more [mouth wash](amenity_1)
- can you send me [1](number_1) [shoe wax](amenity_1) along with [5] (number_2) [shampoo](amenity_2) and [2](number_3) [towels](amenity_3)
- i want [1](number_1) [shoe wax](amenity_1) along with [5] (number_2) [shampoo](amenity_2) and [2](number_3) [towels](amenity_3)
- i need [1](number_1) [shoe wax](amenity_1) along with [5] (number_2) [shampoo](amenity_2) and [2](number_3) [towels](amenity_3)
- bring me a [shoe wax](amenity_1) along with a [shampoo](amenity_2) and a [towels](amenity_3)
- send me a [shoe wax](amenity_1) along with a [shampoo](amenity_2) and a [towels](amenity_3)
- send me [1](number_1) [shoe wax](amenity_1) along with [5] (number_2) [shampoo](amenity_2) and [2](number_3) [towels](amenity_3)
- get me a [grooming kit](amenity_1) along with a [shampoo](amenity_2) and a [towels](amenity_3)
- get me [1](number_1) [grooming kit](amenity_1) along with [5] (number_2) [shampoo](amenity_2) and [2](number_3) [towels](amenity_3)
- send me [grooming kit](amenity_1) and [shampoo](amenity_2) and [towels](amenity_3)
- send me [grooming kit](amenity_1) and [shampoo](amenity_2)
- send me [vanity kit](amenity_1) [shoe wax](amenity_2)
- send me [2](number_1) [vanity kit](amenity_1) along with [shoe wax](amenity_2) [towels](amenity_3)
- send me [2](number_1) [vanity kit](amenity_1) along with [5] (number_2) [shoe wax](amenity_2)
- send me [2](number_1) [vanity kit](amenity_1) along with [shoe wax](amenity_2) and [2](number_3) [towels](amenity_3)
- send me [2](number_1) [vanity kit](amenity_1) along with [shoe wax](amenity_2) and [towels](amenity_3)
- send me [2](number_1) [vanity kit](amenity_1) along with [shoe wax](amenity_2)
- can you send me [2](number_1) [vanity kit](amenity_1) along with [shoe wax](amenity_2)
- send me [2](number_1) [vanity kit](amenity_1)
- could you please bring [vanity kit](amenity_1) along with [2] (number_2) [shoe wax](amenity_2)
- could you please bring [facial scrub](amenity_1) with [2] (number_2) [shoe wax](amenity_2)
- could you please bring [facial scrub](amenity_1) along with [newspaper](amenity_2)
- could you please bring [facial scrub](amenity_1)
- could you please bring [2](number_1) [facial scrub](amenity_1)
- can you please bring [facial scrub](amenity_1) along with [1] (number_2) [newspaper](amenity_2)
- can you please bring [facial scrub](amenity_1) with [1] (number_2) [newspaper](amenity_2)
- can you please bring [facial scrub](amenity_1) along with [newspaper](amenity_2)
- can you please bring [facial scrub](amenity_1)
- can you please bring [2](number_1) [nail cutter](amenity_1)
- please bring [nail cutter](amenity_1) along with [1] (number_2) [newspaper](amenity_2)
- please bring [nail cutter](amenity_1) with [1] (number_2) [newspaper](amenity_2)
- please bring [nail cutter](amenity_1) along with [newspaper](amenity_2)
- please bring [nail cutter](amenity_1)
- please bring [3](number_1) [nail cutter](amenity_1)
- can you please bring me [3](number_1) [nail cutter](amenity_1)
- can you bring me [3](number_1) [nail cutter](amenity_1)
- please bring me [3](number_1) [nail cutter](amenity_1)
- please send me [3](number_1) [nail cutter](amenity_1) and [2] (number_2) [newspaper](amenity_2)
- can i have [3](number_1) [nail filer](amenity_1) and [2] (number_2) [newspaper](amenity_2)
- could you please send me [3](number_1) [nail filer](amenity_1) and [2] (number_2) [newspaper](amenity_2)
- can you send me [3](number_1) [nail filer](amenity_1) and [2] (number_2) [newspaper](amenity_2)
- send me [3](number_1) [nail filer](amenity_1) and [2] (number_2) [newspaper](amenity_2)
- I need a [shampoo](amenity_1)
- i need [shampoo](amenity_1) [towel](amenity_2)
- i need [shampoo](amenity_1) [tissues](amenity_2)
- [2](number_1) [towels](amenity_1)
- I need [4](number_1) [dental kit](amenity_1) and [1](number_2) [sewing kit](amenity_2)
- i wanted few [towels](amenity_1) before i go for bath

## intent:show_menu_intent
- show me the menu
- show me the spa menu
- show me the food menu
- show me the laundry menu
- show me the beverage menu

## intent:location_intent
- can you tell me where is the [spa](location_item) in this hotel
- i want to know where is the [spa](location_item) can you tell me
- i want to know where is the [pool](location_item) in the property
- i want to know where is the [gym](location_item) in the hotel
- i want to know where is the [spa](location_item)
- i want to know where is [swimming pool](location_item:pool)
- can you tell me where is [fitness center](location_item:gym) in the hotel
- can you tell me where is [health club](location_item:gym) in hotel
- do you have [spa](location_item) in the hotel
- do you have [spa](location_item) on property
- where can i find [spa](location_item) in the hotel
- where is [spa](location_item) in hotel
- where is [spa](location_item) in the hotel
- where is [spa](location_item) on property
- can I know where is the [spa](location_item) located
- can you tell where is the [spa](location_item) located
- can you let me know where is the [spa](location_item) located in the hotel
- can you let me know where is the [spa](location_item) located in the property
- can you let me know where is the [spa](location_item) located
- can you tell me where is the [spa](location_item) located in the property
- can you tell me where is the [spa](location_item) located in the hotel
- can you tell me where is the [spa](location_item) located
- is there [spa](location_item) in this hotel
- is there a [spa](location_item) on property
- is there a [spa](location_item) in the property
- is there a [spa](location_item) in the hotel
- is there a [spa](location_item)
- do you have a [spa](location_item) on hotel property
- do you have a [spa](location_item) on property
- do you have a [spa](location_item)
- if there is a [spa](location_item) in hotel
- if there is [spa](location_item) in hotel
- do you have [spa](location_item)
- do you have [spa](location_item) in hotel
- there is a [spa](location_item) in this hotel
- there is a [spa](location_item) in this property
- is there a [spa](location_item) in this property
- is there a [spa](location_item) in this hotel
- if there is a [spa](location_item) in this hotel
- if there is a [spa](location_item) on property
- can you tell me where is the [spa](location_item)
- can you tell me where is [spa](location_item)
- can you let me know where is the [spa](location_item)
- can you let me know where is [spa](location_item)
- can you let me know where can i find [spa](location_item)
- could you please tell me where can i find [spa](location_item)
- can you tell me where can i find [spa](location_item)
- could you please tell me where is [spa](location_item) on property
- could you please tell me where is [spa](location_item) in this hotel
- could you please tell me where is [spa](location_item) in hotel
- could you please tell me where is [spa](location_item)
- where can i find [spa](location_item) here
- where can i find [spa](location_item) in hotel
- where can i find [spa](location_item) in this hotel
- where can i find [spa](location_item) on property
- where can i find [spa](location_item)
- where is [spa](location_item)
- [where can I go for shopping](location_item)

## intent:nearby_attractions_intent
- what are the places to visit nearby
- place to visit near by
- place to visit nearby
- place to travel in the city
- place to visit
- places to travel around
- where can i travel around in the city
- can i get some good recommendation of places to travel around
- could you recommend me some good places to travel around
- where can i roam around and visit the city
- recommend me some places to travel near by
- recommend me some places to travel
- can you recommend me some good places to travel around
- can you recommend me some good places to travel
- places to visit near by
- nearby places to travel around
- what are the nearby places to travel around
- what are the near by places to travel
- what are the nearby places to travel
- what are the places to visit near by
- where can i visit nearby
- where can i visit near by
- can you show me the near by attractions
- can you show me near by attraction
- can you show me near by places to travel
- can you show me where can i travel near by
- please suggest me some places to travel
- could you suggest me some places to travel
- show me where can i travel near by
- show me near by places to travel
- show me near by attraction
- can you tell me about nearby attraction
- can you tell me about near by attraction
- can you tell me about nearby places
- can you tell me about near by places
- can i know near by places
- show me the near by attractions
- could you please tell me the nearby attractions
- what are the nearby attractions
- nearby attractions

## intent:services_intent
- [clean my room](service) can you please send housekeeping department
- [clean my room](service) please send housekeeping department
- send housekeeping department to [change bedsheet](service)
- send someone to [clean my room](service) in the room
- send someone to my room to [clean my room](service)
- could you send someone to [clean my room](service) please
- could you send someone to [clean my room](service)
- i need someone to [clean my room](service)
- [clean my room](service) please
- [my room is a mess](service:clean room)
- i need [new bedsheet](service:change bedsheet)
- can you please send someone to [clean my room](service:clean room)
- i want someone to [clean my room](service)
- i need [clean the washroom](service:clean washroom)
- could you please send someone to [clean my room](service)
- send someone to [replace the cutlery](service:replace cutlery)
- send someone to [replace cutlery](service)

## intent:information_intent
- tell me about [bangalore palace](information)
- tell me about [wild water park](information)
- tell me about [marina mall](information)
- tell me about [hotel](information)
- tell me about [swimming pool](information:pool)
- tell me about [spa](information)
- tell me about [bread street](information)
- tell me about [burj khalifa](information)
- tell me about the [hotel](information)
- tell me about [burj](information:burj khalifa)
- tell me more about [mall](information:dubai marina mall)
- what is [water park](information:wild water park)
- tell me about [water park](information)
- tell me about [Burj khalifa](location_item)

## intent:complaint_intent
- could you please inform the hotel department that [food quality](complaint)
- could you please inform the hotel that [water leakage in the washroom](complaint:tap leakage)
- could you please inform that [drapes are not working](complaint:problem with curtains)
- could you please tell hotel that [t. v. is not working](complaint:television not working)
- could you please tell hotel that [shower is not working](complaint:shower leakage)
- there is some [problem with wifi](complaint:wifi connection)
- that there are [food quality](complaint)
- that there is some [food quality](complaint)
- that there is [food quality](complaint)
- that [food quality](complaint)
- that my [food quality](complaint)
- my [air conditioner is not working](complaint:problem with air conditioner)
- i have a complaint regarding [food quality](complaint)
- there was a [food quality](complaint)
- there is [food quality](complaint)
- i did not like [food quality](complaint)
- i didn't like [food](complaint:food quality)
- [food is burnt](complaint:food overcooked)
- [food is frozen](complaint:cold food)
- [food taste awful](complaint:food taste)
- [food is raw](complaint:food uncooked)
- the food delivered was [uncooked](complaint:food uncooked)
- the food delivered was [burnt](complaint:food overcooked)
- the food delivered was [tasteless](complaint:food taste)
- the food delivered was [frozen](complaint:cold food)
- the food delivered was [raw](complaint:food uncooked)

## intent:food_intent
- i said add [1](number_1) more [lamberhurst estate bacchus](food_1)
- i said add [1](number_1) [lamberhurst estate](food_1:lamberhurst estate bacchus)
- please add [1](number_1) more [lamberhurst wine](food_1:lamberhurst estate bacchus)
- can you add [1](number_1) more [lamberhurst](food_1:lamberhurst estate bacchus) please
- i need [1](number_1) more [long island ice tea](food_1)
- i want [1](number_1) more [L. I. I. T.](food_1:long island ice tea)
- i want to add [1](number_1) more [dirty martini](food_1)
- [1](number_1) of [martini](food_1:dirty martini)
- just [moroccan mint tea](food_1)
- just [1](number_1) [moroccan mint tea](food_1)
- can i get [1](number_1) [moroccan mint tea](food_1) with [less sugar](withCustomization_a)
- [1](number_1) [moroccan mint tea](food_1) with [less sugar](withCustomization_a)
- can i have [moroccan mint tea](food_1) along with [grilled tuna steak](food_2)
- can i have [jalapeno cheese poppers](food_1) with [1](number_2) [grilled tuna steak](food_2)
- can i have [jalapeno cheese poppers](food_1) with [grilled tuna steak](food_2)
- can you order [1](number_1) [jalapeno cheese poppers](food_1)
- add [jalapeno cheese poppers](food_1)
- add [1](number_1) [pina colada](food_1)
- can i have [1](number_1) [pina colada](food_1) with [less sugar](withCustomization_a) [1](number_2) [grilled tuna steak](food_2) with [less sugar](withCustomization_b) and [6](number_3) [tiramisu](food_3) with [less sugar](withCustomization_c)
- can i have [1](number_1) [pina colada](food_1) with [less sugar](withCustomization_a) and [2](number_2)[chocolate truffle](food_2) with [less sugar](withCustomization_b)
- can i have [1](number_1) [pina colada](food_1) with [less sugar](withCustomization_a)
- add [mojito](food_1) and a [chocolate truffle](food_2) and [tiramisu](food_3)
- add [mojito](food_1) and [chocolate truffle](food_2) and [tiramisu](food_3)
- order [mojito](food_1) and [chocolate truffle](food_2) and [tiramisu](food_3)
- order [royal salute](food_1) and [chocolate truffle](food_2)
- order a [royal salute](food_1) and a [nasi goreng](food_2)
- order [2](number_1) [royal salute](food_1) and [2](number_2) [nasi goreng](food_2)
- order [2](number_1) [jameson](food_1) [2](number_2) [nasi goreng](food_2)
- order [2](number_1) [jameson](food_1) [nasi goreng](food_2) and [6](number_3) [tiramisu](food_3)
- order [2](number_1) [jameson](food_1) and [nasi goreng](food_2)
- [jameson](food_1) [nasi goreng](food_2) and [6](number_3) [tiramisu](food_3)
- [caesar salad](food_1) [orange juice](food_2) and [tiramisu](food_3)
- [caesar salad](food_1) [orange juice](food_2) [tiramisu](food_3)
- [2](number_1) [caesar salad](food_1) [orange juice](food_2) and [tiramisu](food_3)
- add [2](number_1) [caesar salad](food_1) and [orange juice](food_2) and [tiramisu](food_3)
- add [2](number_1) [caesar salad](food_1) and [orange juice](food_2)
- can i get a glass of [johnnie walker blue label](food_1)
- can i get a cup of [johnnie walker blue label](food_1)
- [2](number_1) cup of [johnnie walker blue label](food_1)
- [2](number_1) glass of [johnnie walker blue label](food_1)
- can i have a glass of [french fries](food_1)
- can i have a cup of [french fries](food_1)
- can i get [2](number_1) [french fries](food_1)
- get me [2](number_1) [french fries](food_1) along with a [dirty martini](food_2)
- can i have [2](number_1) [nachos](food_1) along with [2](number_2) [dirty martini](food_2)
- can i have [2](number_1) [nachos](food_1) with [2](number_2) [dirty martini](food_2)
- can i have [2](number_1) [nachos](food_1) with [dirty martini](food_2)
- can i have [3](number_1) [nachos](food_1) and [2](number_2) [dirty martini](food_2)
- can i have [3](number_1) [nachos](food_1)
- get me a [nachos](food_1)
- get me [dirty martini](food_1)
- send me [3](number_1) [nachos](food_1) [3](number_2) [dirty martini](food_2) and [8](number_3) [tiramisu](food_3)
- send me [3](number_1) [fried calamari](food_1) [3](number_2) [dirty martini](food_2) and [tiramisu](food_3)
- send me [3](number_1) [fried calamari](food_1) [3](number_2) [dirty martini](food_2)
- send me [3](number_1) [fried calamari](food_1) and [3](number_2) [jasmine tea](food_2)
- send me [3](number_1) [fried calamari](food_1) please
- send me [fried calamari](food_1) please
- could you add [3](number_1) [jalapeno cheese poppers](food_1) and [3](number_2) [jasmine tea](food_2)
- could you add [3](number_1) [jalapeno cheese poppers](food_1)
- send me [4](number_1) [jalapeno cheese poppers](food_1)
- send me a [jalapeno cheese poppers](food_1) please
- send me a [torres coronas tempranillo](food_1)
- [4](number_1) [torres coronas tempranillo](food_1) and [3](number_2) [jasmine tea](food_2)
- order [4](number_1) [torres coronas tempranillo](food_1) [3](number_2) [french fries](food_2) [8](number_3) [tiramisu](food_3)
- i need [4](number_1) [torres coronas tempranillo](food_1)
- [4](number_1) more [torres coronas tempranillo](food_1)
- add [4](number_1) more [torres coronas tempranillo](food_1) to my food cart
- add [4](number_1) more [jacob creek pinot grigio](food_1) to my cart
- add [5](number_1) more [jacob creek pinot grigio](food_1) to the cart
- add [5](number_1) more [jacob creek pinot grigio](food_1) to the food cart
- i would like to have a glass of [jacob creek pinot grigio](food_1)
- [5](number_1) [jacob creek pinot grigio](food_1) please
- [jacob creek pinot grigio](food_1)
- i want [jacob creek pinot grigio](food_1)
- i'd love to have [5](number_1) [quinoa salad](food_1)
- get me [quinoa salad](food_1)
- i need [5](number_1) [quinoa salad](food_1) please
- i need [quinoa salad](food_1)
- to get me  [5](number_1) [quinoa salad](food_1)
- to place an order for [6](number_1) [quinoa salad](food_1) please
- to please place an order for [6](number_1) [quinoa salad](food_1)
- please place an order for [6](number_1) [quinoa salad](food_1)
- place an order for [6](number_1) [quinoa salad](food_1) please
- place an order for [6](number_1) [quinoa salad](food_1)
- please place an order for a [quinoa salad](food_1)
- place an order for a [quinoa salad](food_1) please
- place an order for a [iceberg wedge salad](food_1)
- to please place an order for a [iceberg wedge salad](food_1)
- to place an order for a [iceberg wedge salad](food_1) please
- to place an order for [7](number_1) [iceberg wedge salad](food_1)
- to place an order for a [iceberg wedge salad](food_1)
- yes add [7](number_1) [iceberg wedge salad](food_1) please
- yes add [7](number_1) [iceberg wedge salad](food_1)
- yes add [cappuccino](food_1)
- order [7](number_1) [cappuccino](food_1) please
- yes order [7](number_1) [cappuccino](food_1)
- i would like to have [7](number_1) [cappuccino](food_1) please
- i wanna order [7](number_1) [cappuccino](food_1)
- i would like to have [7](number_1) [cappuccino](food_1)
- i would like to have [espresso](food_1)
- and [8](number_1) [espresso](food_1)
- i want to have [espresso](food_1)
- i would like to have a [espresso](food_1)
- order [espresso](food_1)
- to order [espresso](food_1)
- please add [espresso](food_1)
- i want to have [8](number_1) [espresso](food_1)
- to add [americano](food_1)
- yes [americano](food_1) please
- yes [americano](food_1)
- get [8](number_1) [americano](food_1)
- add [8](number_1) [americano](food_1) as well
- get me [8](number_1) [americano](food_1) along with [3](number_2) [french fries](food_2)
- get me [8](number_1) [nasi goreng](food_1) along with [french fries](food_2)
- get me [8](number_1) [nasi goreng](food_1) and [3](number_2) [french fries](food_2)
- get me [8](number_1) [nasi goreng](food_1)
- can you add [8](number_1) more [nasi goreng](food_1)
- add [9](number_1) [nasi goreng](food_1) with [less sugar](withCustomization_a) and [3](number_2) [french fries](food_2)
- add [9](number_1) [nasi goreng](food_1) [4](number_2) [french fries](food_2) and [8](number_3) [tiramisu](food_3)
- add [9](number_1) [chocolate truffle](food_1) and [4](number_2) [french fries](food_2)
- [9](number_1) [chocolate truffle](food_1) [4](number_2) [french fries](food_2) [8](number_3) [tiramisu](food_3)
- [9](number_1) [chocolate truffle](food_1) [4](number_2) [french fries](food_2) and [8](number_3) [tiramisu](food_3)
- [9](number_1) [chocolate truffle](food_1) [4](number_2) [french fries](food_2)
- i want [9](number_1) [chocolate truffle](food_1) and [4](number_2) [french fries](food_2)
- i want [9](number_1) [chocolate truffle](food_1) [4](number_2) [french fries](food_2)
- i want [9](number_1) [chocolate truffle](food_1) [4](number_2) [french fries](food_2) and [1](number_3) [tiramisu](food_3)
- i want [9](number_1) [tiramisu](food_1) [4](number_2) [french fries](food_2) [1](number_3) [tiramisu](food_3)
- i need [9](number_1) [tiramisu](food_1) [6](number_2) [french fries](food_2) and [1](number_3) [tiramisu](food_3)
- i need [9](number_1) [tiramisu](food_1) [6](number_2) [french fries](food_2)
- i need [9](number_1) [tiramisu](food_1) [6](number_2) [french fries](food_2) [1](number_3) [tiramisu](food_3)
- i need [9](number_1) [tiramisu](food_1) and [6](number_2) [french fries](food_2)
- can I get a [tiramisu](food_1) please with [less sugar](withCustomization_a)
- can I have [tiramisu](food_1) [less sugar](withCustomization_a)
- then can I have [tiramisu](food_1) please
- then can I have [green tea](food_1)
- alright then can I have [green tea](food_1)
- alright then can I have [green tea](food_1) please
- can I order [green tea](food_1)
- may I have [green tea](food_1)
- do you have [green tea](food_1)
- can I get [green tea](food_1) and [french fries](food_2)
- can I get [jasmine tea](food_1)
- i want [jasmine tea](food_1) [less sugar](withCustomization_a)
- i'd like to order [jasmine tea](food_1) with [mojito](food_2)
- i'd like to order [9](number_1) [jasmine tea](food_1) [1](number_2) [mojito](food_2) and [1](number_3) [mojito](food_3)
- i'd like to order [9](number_1) [jasmine tea](food_1) and [1](number_2) [mojito](food_2)
- i'd like to order [9](number_1) [jasmine tea](food_1)
- i'd like to order [jasmine tea](food_1) with [mojito](food_2) and [mojito](food_3)
- i'd like to order [jasmine tea](food_1)
- i'd like to order [orange juice](food_1) and [mojito](food_2)
- add [orange juice](food_1) [mojito](food_2) and [tiramisu](food_3)
- add [9](number_1) [orange juice](food_1) [mojito](food_2) and [tiramisu](food_3)
- can you please add [9](number_1) [orange juice](food_1)
- order [9](number_1) [orange juice](food_1)
- i want [9](number_1) [orange juice](food_1) and [1](number_2) [mojito](food_2) and [1](number_3) [tiramisu](food_3)
- i want [9](number_1) [orange juice](food_1) with [mojito](food_2) and [tiramisu](food_3)
- i want [9](number_1) [mango juice](food_1) with [mojito](food_2)
- i want [9](number_1) [mango juice](food_1)
- could you please add [9](number_1) [mango juice](food_1)
- please add [9](number_1) [mango juice](food_1)
- i want [9](number_1) [mojito](food_1)
- add [9](number_1) more [grilled tuna steak](food_1)
- [9](number_1) [grilled tuna steak](food_1)
- i want to order [9](number_1) [grilled tuna steak](food_1)
- i want to order [grilled tuna steak](food_1)
- i would like to order [9](number_1) [grilled tuna steak](food_1)
- i would like to order [grilled tuna steak](food_1)
- I want [mojito](food_1)
- I want [mango juice](food_1) also
- I want [mango juice](food_1) and [orange juice](food_2)
- I want [mango juice](food_1)
- I want [1](number_1) [mojito](food_1)

## intent:wifi_intent
- may i know how can i connect to wifi please
- let me know how can i connect wifi
- let me know how can i connect to wifi
- how i connect with wifi
- may i know how can i connect with wifi
- may i know how can i connect to wifi
- may i know how can i connect wifi
- can you please let me know how do i connect wifi
- can you please let me know how can i connect to wifi
- can you please let me know how can i connect wifi
- show me the wifi password
- show me wifi password
- tell me the wifi password
- can you tell me the wifi password please
- can you please tell me the wifi password
- could you please tell me the wifi password
- wifi password please
- what is wifi password
- tell me wifi password
- if there is a wifi in this room
- if there is a wifi on property
- if there is a wifi
- can you please tell me how to connect to hotel wifi
- can you please tell me how to connect to the hotel wifi
- can you please tell me how to connect to the hotel's wifi
- can you please tell me how to connect to hotel's wifi
- can you please tell me how to connect to the net
- can you please tell me how to connect to net
- can you please tell me how to connect to the internet
- can you please tell me how to connect to internet
- can you please tell me how to connect to wifi
- could you please tell me how to connect to hotel wifi
- could you please tell me how to connect to hotel's wifi
- could you please tell me how to connect to net
- could you please tell me how to connect to internet
- could you please tell me how to connect to wifi
- is their wifi in this hotel how to connect to it
- is their wifi in this hotel how to connect
- is there wifi in this hotel
- do you have wifi in the hotel
- do you have wifi in hotel
- do you have wifi in this hotel
- is their wifi in this room how to connect to it
- is their wifi in this room how to connect
- is there wifi in this room
- do you have wifi in room
- do you have wifi
- is the wifi working
- how to do connection with wifi
- how to do connection with net
- how to do connection with internet
- connect with wifi
- how can i connect with the internet
- how can i connect with internet
- how can i connect with the wifi
- how can i connect with wifi
- how can i connect with the net
- how can i connect with net
- how do i connect with the net
- how do i connect with net
- how do i connect with wifi
- how do i connect with the wifi
- how do i connect with the internet
- how do i connect with internet
- help me connect to the wifi
- please help me connect to the wifi
- could you please help me connect to internet
- could you please help me connect to the internet
- could you please help me connect to the wifi
- help me connect with net
- help me connect with internet
- help me connect with wifi
- help me connect with the wifi
- could you please help me connect to wifi
- please help me connect to wifi
- help me please i want to connect to wifi
- help me i want to connect to wifi
- i want to connect to wifi help me
- i want to connect to wifi help me with it
- i want to connect to wifi can you help me with it
- i want to connect to wifi can you please help me with it
- i want to connect to wifi could you please help me with it
- i want to connect to wifi could you please help me
- could you please tell me how do i connect to wifi
- could you tell me how do i connect to wifi
- can you tell me how do i connect to wifi
- can I know how do I connect to wifi
- i want to connect to wifi
- can you help me connect with wifi
- how to connect to wi fi
- what is the wi fi password
- how do i connect to wi fi
- help me connect to wi fi
- how can i connect to wi fi
- help me connect to wifi
- how can i connect to wifi
- how do i connect to internet
- how do i connect to wifi
- how to connect to the internet
- how to connect to wifi
- what is the wifi password

## intent:remove_food_intent
- remove [1](remove_number_1) [mango juice](food_1) from my cart
- remove [1](remove_number_1) [mango juice](food_1) from my food cart
- i want {addNumber_a} {addFood_a} not [1](remove_number_1) [mango juice](food_1)
- i want {addNumber_a} {addFood_a} not [mango juice](food_1)
- i want {addFood_a} not [mango juice](food_1)
- i don't want [1](remove_number_1) [mango juice](food_1) i want {addNumber_a} {addFood_a}
- i don't want [mango juice](food_1) i want {addNumber_a} {addFood_a}
- i said i don't want [mango juice](food_1) i want {addFood_a}
- i don't want [mango juice](food_1) i want {addFood_a}
- i don't want [mango juice](food_1)
- can i you please remove [mango juice](food_1) [orange juice](food_2) and [pina colada](food_3)
- can i you please remove [mango juice](food_1) and [orange juice](food_2)
- can i you please remove [mango juice](food_1)
- can i you please remove [1](remove_number_1) [mango juice](food_1)
- remove [mango juice](food_1) [orange juice](food_2) and [pina colada](food_3)
- remove [mango juice](food_1) and [orange juice](food_2)
- remove [mango juice](food_1)
- remove [1](remove_number_1) [mango juice](food_1)
- remove [mango juice](food_1) and [dirty martini](food_2)
- remove [1](remove_number_1) [mango juice](food_1) and [2](remove_number_2) [orange juice](food_2)

## intent:book_spa_intent
- may I get spa booking from the hotel
- may I get spa assistance from the hotel
- can i book the spa
- make a spare reservation
- go for [bespoke facial](spa_services)
- make a spur reservation
- make a spa slot reservation
- make a spa reservation
- i am tired can you book me a spa
- i am tired can you book me a [bespoke facial](spa_services) for tonight at 7 P.M.
- i am tired can you book me a [bespoke facial](spa_services)
- can you book me a spa for 9 AM
- can you book me a spa also
- can you book me a spa
- [bespoke facial](spa_services) for morning
- [bespoke facial](spa_services) at 9 AM for morning
- can you book [arabian rose ritual](spa_services) at 7 PM for today
- can you book [arabian rose ritual](spa_services) at 7 PM on today
- can you book [arabian rose ritual](spa_services) at 7 PM today
- can you book [arabian rose ritual](spa_services) at 7 PM
- can you book [arabian rose ritual](spa_services)
- [arabian rose ritual](spa_services) at 7 PM on today
- [arabian rose ritual](spa_services) at 7 PM today
- [arabian rose ritual](spa_services) at 7 PM
- [arabian rose ritual](spa_services) and {SpaService_b}
- [arabian rose ritual](spa_services)
- can you book spa for me
- can you book spa
- could you do a spa booking for [arabian rose ritual](spa_services) and {SpaService_b} at 7 PM in the evening on today
- could you do a spa booking for [arabian rose ritual](spa_services) and {SpaService_b} at 7 PM in the evening today
- could you do a spa booking for [arabian rose ritual](spa_services) and {SpaService_b} at 7 PM in the evening
- could you do a spa booking for [arabian rose ritual](spa_services) and {SpaService_b}
- could you do a spa booking for [arabian rose ritual](spa_services)
- could you do a spa booking for me
- could you do a spa booking for me please
- could you do a spa booking
- take the spa booking
- take a spa booking for [arabian rose ritual](spa_services) and {SpaService_b} at evening 7 PM today
- take a spa booking for [arabian rose ritual](spa_services) and {SpaService_b} at evening 7 PM on today
- take a spa booking for [total sleep ritual](spa_services) and {SpaService_b} at evening 7 PM
- take a spa booking for [total sleep ritual](spa_services) and {SpaService_b}
- take a spa booking for [total sleep ritual](spa_services)
- take a spa booking for today at evening 7 PM
- take a spa booking for today
- take a spa booking
- book a spa service
- book a [total sleep ritual](spa_services) at evening 7 PM on next monday
- can you help me book [total sleep ritual](spa_services) at evening 7 PM
- can you help me book [total sleep ritual](spa_services) on next monday
- can you help me book [total sleep ritual](spa_services) next monday
- can you help me book [total sleep ritual](spa_services)
- can i book [total sleep ritual](spa_services) evening 7 PM
- can i book [total sleep ritual](spa_services) next monday
- can i book [total sleep ritual](spa_services) at evening 7 PM next monday
- can i book [total sleep ritual](spa_services) at evening 7 PM
- can i book [total sleep ritual](spa_services)
- can you book me a [total sleep ritual](spa_services) at evening 7 PM next monday
- can you book me a [total sleep ritual](spa_services) at evening 7 PM
- can you book me a [total sleep ritual](spa_services) next monday
- can you book me a [total sleep ritual](spa_services)
- book me a [total sleep ritual](spa_services)
- book [total sleep ritual](spa_services) at evening 7 PM for next monday
- book [total sleep ritual](spa_services) at evening 7 PM next monday
- book a [total sleep ritual](spa_services) at evening 7 PM for next monday
- book a [total sleep ritual](spa_services) at evening 7 PM next monday
- can i get a booking at the spa for [total sleep ritual](spa_services) and {SpaService_b} at 1 PM for next monday
- can i get a booking at the spa for [total sleep ritual](spa_services) and {SpaService_b} at 1 PM next monday
- can i get a booking at the spa for [total sleep ritual](spa_services) and {SpaService_b} at 1 PM
- can i get a booking at the spa for [total sleep ritual](spa_services) and {SpaService_b}
- can i get a booking at the spa for 1 PM
- can i get a booking at the spa for next weekend
- can i get a booking at the spa for [1](number_1)
- can i get a booking at the spa for [total sleep ritual](spa_services) at 1 PM
- can i get a booking at the spa for [total sleep ritual](spa_services)
- can i get a booking at the spa
- can i get a booking slot at the spa
- can i get a slot at the spa
- reserve a slot at the spa for [total sleep ritual](spa_services)
- reserve a slot at the spa for [total sleep ritual](spa_services) and {SpaService_b}
- reserve a slot at the spa at 1 PM for [total sleep ritual](spa_services)
- reserve a slot at the spa at 1 PM for [total sleep ritual](spa_services) and {SpaService_b}
- i want to reserve a slot at the spa at 1 PM for [total sleep ritual](spa_services) and {SpaService_b}
- i want to reserve a slot at the spa at 1 PM for [total sleep ritual](spa_services)
- i want to reserve a slot at the spa at 1 PM
- i want to reserve a slot at the spa next weekend
- book a [total sleep ritual](spa_services) and {SpaService_b} at 1 PM on next weekend
- book a [total sleep ritual](spa_services) and {SpaService_b} at 1 PM next weekend
- book a [deep muscle massage](spa_services) and {SpaService_b} at 1 PM
- book a [deep muscle massage](spa_services) and {SpaService_b}
- book a [deep muscle massage](spa_services) at 1 PM for [1](number_1)
- book a [deep muscle massage](spa_services) and a {SpaService_b}
- book [deep muscle massage](spa_services) and {SpaService_b}
- book an [deep muscle massage](spa_services) and an {SpaService_b}
- book [deep muscle massage](spa_services) and a {SpaService_b}
- book a [deep muscle massage](spa_services) and a {SpaService_b} at 1 PM
- book [deep muscle massage](spa_services) and {SpaService_b} at 1 PM
- book an [deep muscle massage](spa_services) and an {SpaService_b} at 9 AM
- book [deep muscle massage](spa_services) and a {SpaService_b} at 9 AM
- book a [deep muscle massage](spa_services) and a {SpaService_b} next weekend at 9 AM
- book [deep muscle massage](spa_services) and {SpaService_b} next weekend at 9 AM
- book an [deep muscle massage](spa_services) and an {SpaService_b} next weekend at 9 AM
- book [deep muscle massage](spa_services) and a {SpaService_b} next weekend at 9 AM
- book a [deep muscle massage](spa_services) and a {SpaService_b} for next weekend at 9 AM
- book [deep muscle massage](spa_services) and {SpaService_b} for next weekend at 9 AM
- book an [deep muscle massage](spa_services) and an {SpaService_b} for tomorrow at 9 AM
- book [deep muscle massage](spa_services) and a {SpaService_b} for tomorrow at 9 AM
- book a [deep muscle massage](spa_services) and a {SpaService_b} for tomorrow
- book [deep muscle massage](spa_services) and {SpaService_b} for tomorrow
- book an [deep muscle massage](spa_services) and an {SpaService_b} for tomorrow
- book [deep muscle massage](spa_services) and a {SpaService_b} for tomorrow
- book a [deep muscle massage](spa_services) for [1](number_1)
- book a [deep muscle massage](spa_services) at 9 AM
- book a [deep muscle massage](spa_services) tomorrow
- book a [deep muscle massage](spa_services) for tomorrow
- book a [deep muscle massage](spa_services) for tomorrow at 9 AM
- book a [deep muscle massage](spa_services) tomorrow at 9 AM for [1](number_1)
- book [deep muscle massage](spa_services) for [1](number_1)
- book [deep muscle massage](spa_services) at 9 AM
- book [deep muscle massage](spa_services) tomorrow
- book [deep muscle massage](spa_services) for tomorrow
- book [deep muscle massage](spa_services) for tomorrow at 9 AM
- book [deep muscle massage](spa_services) day after tomorrow at 9 AM for [1](number_1)
- book a [deep muscle massage](spa_services)
- make a spa reservation for [deep muscle massage](spa_services) day after tomorrow at 9 AM
- make a spa reservation for [deep muscle massage](spa_services) at 9 AM
- make a spa reservation for [energy balancing treatment](spa_services) for [1](number_1)
- make a spa reservation for [energy balancing treatment](spa_services) for day after tomorrow
- make a spa reservation for [energy balancing treatment](spa_services)
- make a reservation for [energy balancing treatment](spa_services) at 9 AM
- make a reservation for [energy balancing treatment](spa_services)
- book me a [energy balancing treatment](spa_services) at 9 AM
- book me a [energy balancing treatment](spa_services) for [1](number_1)
- make a spa reservation for [1](number_1)
- make a spa reservation at 9 AM
- make a spa reservation at 9 AM day after tomorrow
- yes book a spa slot at 9 AM day after tomorrow please
- yes please book a spa slot at 9 AM day after tomorrow
- yes book a spa slot at 9 AM day after tomorrow
- to book a spa slot at 9 AM day after tomorrow please
- to please book a spa slot at 9 AM day after tomorrow
- book a spa slot at 9 AM day after tomorrow please
- please book a spa slot at 9 AM day after tomorrow
- to book a spa slot at 9 AM day after tomorrow
- book a spa slot at 9 AM day after tomorrow
- book spa slot for me
- book a spa slot for me
- to reserve a spa slot for me please
- to reserve a spa slot for me
- to reserve a spa slot
- to book a spa slot for me please
- to book a spa slot for me
- Great. Reserve a session at 9 AM day after tomorrow please
- Awesome. Reserve a session at 9 AM day after tomorrow
- to Reserve a session at 9 AM day after tomorrow
- Reserve a session at 9 AM day after tomorrow
- Sure. Reserve a session at 9 AM day after tomorrow
- Great. Reserve a session at 9 AM day after tomorrow
- spa reservation please
- spa reservation
- book a spa slot for [1](number_1) for day after tomorrow
- book a spa slot for [1](number_1) at 9 AM
- please book a spa slot for [1](number_1)
- book a spa slot for [1](number_1)
- please book a slot for [1](number_1) at 9 AM day after tomorrow at the spa
- book a slot for [1](number_1) at 9 AM day after tomorrow at the spa please
- book a slot for [1](number_1) at 9 AM morning at the spa
- to please book a slot for [1](number_1) at 9 AM morning at the spa
- to book a slot for [1](number_1) at 9 AM morning at the spa please
- to book a slot for [1](number_1) at 9 AM morning at the spa
- i want to book a spa slot on morning at 9 AM for [1](number_1)
- i want to book a spa slot on morning at 9 AM
- i want to book a spa slot for morning 9 AM
- i want to book a spa slot for [1](number_1)
- i want to book a spa slot
- i would like to book a spa slot on morning at 9 AM for [1](number_1)
- i would like to book a spa slot on morning at 9 AM
- i would like to book a spa slot on morning
- i would like to book a spa slot at 9 AM morning
- i would like to book a spa slot for [1](number_1)
- i would like to book a spa slot for 9 AM morning
- i would like to book a spa slot
- to book a spa slot please
- to please book a spa slot
- please book a spa slot
- book a spa slot please
- book a spa slot
- to book a spa slot
- book spa slot
- to book spa
- book spa
- to book me a spa slot for 9 AM morning please
- to please book me a spa slot for 9 AM morning
- to book me a spa slot for 9 AM morning
- please book me a spa slot for 9 AM morning
- book me a spa slot for 9 AM morning please
- book me a spa slot for 9 AM morning
- to book me a spa slot please
- to please book me a spa slot
- to book me a spa slot
- please book me a spa slot
- book me a spa slot please
- book me a spa slot
- book spa at 9 AM morning for [1](number_1)
- please book spa at 9 AM morning
- book a spa at 9 AM on morning please
- book a spa at 9 AM on morning
- book a slot at spa for 9 AM morning
- book a slot at spa for 9 AM morning please
- please book a slot at spa for 9 AM morning
- to book a slot at spa for 9 AM morning
- to book a slot at spa for 9 AM morning please
- to please book a slot at spa for 9 AM morning
- to book spa for 9 AM morning
- book spa for 9 AM morning please
- please book spa for 9 AM morning
- to please book spa for 9 AM morning
- to book spa for 9 AM morning please
- book spa on morning at 9 AM
- book spa on morning at 9 AM please
- please book spa on morning at 9 AM
- to book spa on morning at 9 AM please
- to book spa on morning at 9 AM
- to please book spa on morning at 9 AM
- book spa at 9 AM morning
- book spa at 9 AM morning please
- to book spa at 9 AM morning
- to book spa at 9 AM morning please
- to please book spa at 9 AM today
- book spa for [1](number_1)
- book a spa for [1](number_1)
- book spa for today at 9 AM
- book a spa at 9 AM
- please make a spa reservation
- can i make a spa reservation
- can i book reservation at the spa
- i want to reserve a slot at the spa morning
- can you make a reservation at spa
- can you book me a spa slot
- can you make a spa reservation
- book spa for [2](number_1) at 9 AM today
- to book a slot at Spa for [2](number_1) today
- to book a slot at Spa for [2](number_1) morning please
- to please book a slot at Spa for [2](number_1) morning
- to book a slot at Spa for me morning
- to book a slot at Spa for me morning please
- to please book a slot at Spa for me morning
- to book a slot at Spa for [2](number_1) morning at 9 AM
- to book a slot at Spa for [2](number_1) morning at 9 AM please
- to please book a slot at Spa for [2](number_1) morning at 9 AM
- book a [energy balancing treatment](spa_services)
- book me a [thai massage](spa_services)
- book me a [energy balancing treatment](spa_services)
- i am tired can you book me a [thai massage](spa_services)

## synonym:burj khalifa
- burj

## synonym:caesar salad
- caesar

## synonym:change bedsheet
- new bedsheet

## synonym:chivas regal 18 years old
- chivas
- chivas regal
- chivas regal 18 year
- chivas regal 18 years

## synonym:clean room
- my room is a mess
- clean my room

## synonym:clean washroom
- clean the washroom

## synonym:cold food
- food is frozen
- frozen

## synonym:dirty martini
- martini

## synonym:dubai marina mall
- mall
- where can I go for shopping

## synonym:food overcooked
- food is burnt
- burnt

## synonym:food quality
- food

## synonym:food taste
- tasteless
- food taste awful

## synonym:food uncooked
- food is raw
- raw
- uncooked

## synonym:french fries
- fries

## synonym:fried calamari
- calamari

## synonym:gym
- health club
- fitness center

## synonym:jacob creek pinot grigio
- jacob creek pinot
- pinot grigio
- jacob creek

## synonym:jalapeno cheese poppers
- jalapeno poppers
- cheese poppers

## synonym:jameson
- john jameson

## synonym:johnnie walker blue label
- blue label

## synonym:johnnie walker red label
- red label

## synonym:lamberhurst estate bacchus
- lamberhurst red wine
- lamberhurst wine
- lamberhurst
- lamberhurst estate

## synonym:long island ice tea
- long island tea
- L. I. I. T.
- L. I. T.

## synonym:moroccan mint tea
- moroccan tea
- mint tea
- moroccan

## synonym:pool
- swimming pool

## synonym:problem with air conditioner
- air conditioner is not working

## synonym:problem with curtains
- drapes are not working

## synonym:replace cutlery
- replace the cutlery

## synonym:royal salute
- royal salute whisky
- royal salute whiskey

## synonym:shower leakage
- shower is not working

## synonym:tap leakage
- water leakage in the washroom

## synonym:television not working
- t. v. is not working

## synonym:torres coronas tempranillo
- tempranillo
- torres tempranillo
- coronas
- coronas tempranillo
- torres coronas

## synonym:wifi connection
- problem with wifi

## synonym:wild water park
- water park

## regex:number_1
- [1-9][0-9]?

## regex:number_2
- [1-9][0-9]?

## regex:number_3
- [1-9][0-9]?

## regex:remove_number_1
- [1-9][0-9]?

## regex:remove_number_2
- [1-9][0-9]?

## lookup:amenity_1
  data/lookups/amenity_1.txt

## lookup:amenity_2
  data/lookups/amenity_2.txt

## lookup:amenity_3
  data/lookups/amenity_3.txt

## lookup:complaint
  data/lookups/complaint.txt

## lookup:food_1
  data/lookups/food_1.txt

## lookup:food_2
  data/lookups/food_2.txt

## lookup:food_3
  data/lookups/food_3.txt

## lookup:location_item
  data/lookups/location_item.txt

## lookup:service
  data/lookups/service.txt

## lookup:spa_services
  data/lookups/spa_services.txt