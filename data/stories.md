## happy path
* greet
  - utter_greet
* mood_great
  - utter_happy

## sad path 1
* greet
  - utter_greet
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* affirm
  - utter_happy

## sad path 2
* greet
  - utter_greet
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* deny
  - utter_goodbye

## say goodbye
* goodbye
  - utter_goodbye

## bot challenge
* bot_challenge
  - utter_iamabot

## Amenity

* greet
    - utter_greet
* amenities{"amenity":"towel"}
    - utter_amenity

## New Story

* amenities{"amenity_1":"towel","amenity_2":"shampoo"}
    - utter_amenity

## New Story

* greet
    - utter_greet
* amenities{"amenity_1":"towels"}
    - utter_amenity
* amenities{"amenity_1":"towels","amenity_2":"shampoo"}
    - utter_amenity
